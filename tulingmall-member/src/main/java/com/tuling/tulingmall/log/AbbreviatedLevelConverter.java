package com.tuling.tulingmall.log;


import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.pattern.ClassicConverter;
import ch.qos.logback.classic.spi.ILoggingEvent;

public class AbbreviatedLevelConverter extends ClassicConverter {
    @Override
    public String convert(ILoggingEvent event) {
        Level level = event.getLevel();
        switch (level.toInt()) {
            case Level.ERROR_INT: return "E";
            case Level.WARN_INT:  return "W";
            case Level.INFO_INT:  return "I";
            case Level.DEBUG_INT: return "D";
            case Level.TRACE_INT: return "T";
            default: return "U"; // 未知级别返回 U
        }
    }
}