package com.tuling.snowflake.core;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;

/**
 * Twitter_Snowflake
 * SnowFlake的结构如下(每部分用-分开):
 * 0 - 0000000000 0000000000 0000000000 0000000000 0 - 00000 - 00000 - 000000000000
 * 1位标识，由于long基本类型在Java中是带符号的，最高位是符号位，正数是0，负数是1，所以id一般是正数，最高位是0
 * 41位时间戳(毫秒级)，注意，41位时间戳不是存储当前时间的时间戳，而是存储时间戳的差值（当前时间戳 - 开始时间戳)
 * 得到的值），这里的的开始时间戳，一般是我们的id生成器开始使用的时间，由我们程序来指定的
 * （如下面程序SnowflakeIdWorker类的startTime属性）。
 * 41位的时间戳，可以使用69年，年T = (1L << 41) / (1000L * 60 * 60 * 24 * 365) = 69
 * 10位的数据机器位，可以部署在1024个节点，包括5位datacenterId和5位workerId
 * 12位序列，毫秒内的计数，12位的计数顺序号支持每个节点每毫秒(同一机器，同一时间戳)产生4096个ID序号
 * 加起来刚好64位，为一个Long型。
 */
public class SnowflakeIdWorker {
    private Logger logger = LoggerFactory.getLogger(SnowflakeIdWorker.class);
    /**
     * 序列在id中占的位数
     */
    private final long sequenceBits = 12L;

    /**
     * 数据标识id所占的位数
     */
    private final long datacenterIdBits = 5L;

    /**
     * 机器id所占的位数
     */
    private final long workerIdBits = 5L;


    /**
     * 支持的最大机器id，结果是31 (这个移位算法可以很快的计算出几位二进制数所能表示的最大十进制数)
     */
    private final long maxWorkerId = -1L ^ (-1L << workerIdBits);

    /**
     * 支持的最大数据标识id，结果是31
     */
    private final long maxDatacenterId = -1L ^ (-1L << datacenterIdBits);

    /**
     * 数据标识id位移
     */
    private final long datacenterIdShift = sequenceBits;

    /**
     * 机器ID位移
     */
    private final long workerIdShift = sequenceBits + datacenterIdBits;


    /**
     * 时间戳位移
     */
    private final long timestampLeftShift = sequenceBits + workerIdBits + datacenterIdBits;

    /**
     * 生成序列的掩码，这里为4095，对应二进制 1111 1111 1111
     */
    private final long sequenceMask = -1L ^ (-1L << sequenceBits);

    /**
     * 工作机器ID(0~31)
     */
    private long workerId;

    /**
     * 数据中心ID(0~31)
     */
    private long datacenterId;

    /**
     * 毫秒内序列(0~4095)
     */
    private long sequence = 0L;

    /**
     * 上次生成ID的时间戳
     */
    private long lastTimestamp;
    private String persistLastTimestampFileName;

    //==============================Constructors=====================================

    /**
     * 构造函数
     *
     * @param workerId     工作ID (0~31)
     * @param datacenterId 数据中心ID (0~31)
     */
    public SnowflakeIdWorker(long workerId, long datacenterId, String persistLastTimestampFileName) {
        if (workerId > maxWorkerId || workerId < 0) {
            throw new IllegalArgumentException(String.format("worker Id can't be greater than %d or less than 0", maxWorkerId));
        }
        if (datacenterId > maxDatacenterId || datacenterId < 0) {
            throw new IllegalArgumentException(String.format("datacenter Id can't be greater than %d or less than 0", maxDatacenterId));
        }
        this.workerId = workerId;
        this.datacenterId = datacenterId;
        this.persistLastTimestampFileName = persistLastTimestampFileName;
        this.lastTimestamp = loadLastTimestamp();
    }


    /**
     * 加载已持久化的时间戳
     */
    private long loadLastTimestamp() {
        File file = new File(persistLastTimestampFileName);
        try (InputStreamReader isr = new InputStreamReader(new FileInputStream(file), "GBK");
             BufferedReader reader = new BufferedReader(isr)) {
            String lineData = reader.readLine();
            if (StringUtils.isEmpty(lineData)) return -1L;
            return Long.parseLong(lineData);
        } catch (IOException e) {
            logger.error(e.getMessage());
            return -1L;
        }
    }

    // ==============================Methods==========================================

    /**
     * 获得下一个ID (该方法是线程安全的)
     *
     * @return SnowflakeId
     */
    public synchronized long nextId() {
        long timestamp = System.currentTimeMillis();

        // 时钟回拨
        if (timestamp < lastTimestamp) {
            long offset = lastTimestamp - timestamp;
            // 短时间的时钟回拨：自旋
            if (offset < 5) {
                try {
                    wait(offset << 1);
                    timestamp = System.currentTimeMillis();
                    if (timestamp < lastTimestamp) {
                        throw new RuntimeException(
                                String.format("Clock moved backwards.  Refusing to generate id for %d milliseconds", lastTimestamp - timestamp));
                    }
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            }
            // 一般范围地时钟回拨：让时间戳脱离实际的时钟
            else if (offset < 10) {
                timestamp = lastTimestamp;
            }
            // 长时间的时钟回拨：抛出异常
            else {
                throw new RuntimeException(
                        String.format("Clock moved backwards.  Refusing to generate id for %d milliseconds", lastTimestamp - timestamp));
            }
        }
        // 此时：已经确认了lastTimestamp<=timestamp

        // 同一时间段内请求（毫秒级）
        if (lastTimestamp == timestamp) {
            // 当sequence累加到4096，也就是0001000000000000时，和sequenceMask相与后sequence=0
            sequence = (sequence + 1) & sequenceMask;
            // 毫秒内序列溢出
            if (sequence == 0) {
                // 获得下一个毫秒
                timestamp = tilNextMillis(lastTimestamp);
            }
        }
        // 不同时间段内的请求（时间已经向前推进了，需重置流水号）
        else {
            sequence = 0L;
        }

        // 缓存当前时间戳，用于验证时钟回拨
        lastTimestamp = timestamp;
        asyncPersistLastTimestamp(lastTimestamp);
        // 移位并通过或运算拼到一起组成64位的ID
        return (timestamp << timestampLeftShift)
                | (workerId << workerIdShift)
                | (datacenterId << datacenterIdShift)
                | sequence;
    }

    /**
     * 异步持久化lastTimestamp，放在系统重启无法时，无法验证是否发生时钟回拨
     */
    private void asyncPersistLastTimestamp(long lastTimestamp) {
        File file = new File(persistLastTimestampFileName);
        try (OutputStreamWriter fileWrite = new OutputStreamWriter(new FileOutputStream(file), "GBK");
             BufferedWriter buffWrite = new BufferedWriter(fileWrite)) {
            buffWrite.write(String.valueOf(lastTimestamp));
        } catch (IOException e) {
            logger.error(e.getMessage());
        }
    }

    /**
     * 阻塞到下一个毫秒，直到获得新的时间戳，这里有两种处理方式
     * 1、阻塞当前线程：直到当前时间推进到lastTimestamp之后
     * 2、不阻塞：将lastTimestamp+1，让时间戳脱离实际的系统时钟
     * @param lastTimestamp 上次生成ID的时间戳
     * @return 当前时间戳
     */
    protected long tilNextMillis(long lastTimestamp) {
        long timestamp = System.currentTimeMillis();
        while (timestamp <= lastTimestamp) {
            timestamp = System.currentTimeMillis();
        }
        return timestamp;
    }

}