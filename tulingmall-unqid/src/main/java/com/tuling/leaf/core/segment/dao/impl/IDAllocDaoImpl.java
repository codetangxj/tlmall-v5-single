package com.tuling.leaf.core.segment.dao.impl;

import com.tuling.leaf.core.segment.dao.IDAllocDao;
import com.tuling.leaf.core.segment.dao.IDAllocMapper;
import com.tuling.leaf.core.segment.model.LeafAlloc;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.util.List;


@Repository("IDAllocDaoImpl")
public class IDAllocDaoImpl implements IDAllocDao {

    @Autowired
    private IDAllocMapper idAllocMapper;

    @Override
    @Transactional
    public List<LeafAlloc> getAllLeafAllocs() {
        return idAllocMapper.getAllLeafAllocs();
    }

    @Override
    @Transactional
    public LeafAlloc updateMaxIdAndGetLeafAlloc(String tag) {
        // 更新DB中id的最大值（DB中配置了对应的步长）
        idAllocMapper.updateMaxId(tag);
        // 更新maxId后获取更新后的id
        LeafAlloc result = idAllocMapper.getLeafAlloc(tag);
        return result;
    }

    @Override
    @Transactional
    public LeafAlloc updateMaxIdByCustomStepAndGetLeafAlloc(LeafAlloc leafAlloc) {
        idAllocMapper.updateMaxIdByCustomStep(leafAlloc);
        LeafAlloc result =  idAllocMapper.getLeafAlloc( leafAlloc.getKey());
        return result;
    }

    @Override
    @Transactional
    public List<String> getAllTags() {
        return idAllocMapper.getAllTags();
    }
}
