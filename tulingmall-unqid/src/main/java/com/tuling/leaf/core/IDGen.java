package com.tuling.leaf.core;


import com.tuling.leaf.core.common.Result;

public interface IDGen {
    Result get(String key);
    boolean init();
}
