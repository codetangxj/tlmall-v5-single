package com.tuling.tulingmall.config;

import com.alibaba.druid.pool.DruidDataSource;
import com.tuling.leaf.core.common.PropertyFactory;
import com.tuling.tulingmall.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import javax.sql.DataSource;
import javax.annotation.PostConstruct;
import java.sql.SQLException;
import java.util.Properties;

@Configuration
public class SystemConf {
    private Logger logger = LoggerFactory.getLogger(SystemConf.class);

    @Value("${spring.cloud.nacos.discovery.server-addr}")
    private String nacosDiscoveryAddr;

    @PostConstruct
    public void showConf(){
        logger.info("启动配置 nacosDiscoveryAddr:[{}]",nacosDiscoveryAddr);
    }


    @Bean
    public DataSource clusterDataSource() throws SQLException {
        Properties properties = PropertyFactory.getProperties();
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setUrl(properties.getProperty(Constants.LEAF_JDBC_URL));
        dataSource.setUsername(properties.getProperty(Constants.LEAF_JDBC_USERNAME));
        dataSource.setPassword(properties.getProperty(Constants.LEAF_JDBC_PASSWORD));
        dataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
        dataSource.setValidationQuery("select 1");
        dataSource.init();
        return dataSource;
    }
}
