package com.tuling.tulingmall.controller;

import com.tuling.leaf.core.common.Result;
import com.tuling.leaf.core.common.Status;
import com.tuling.tulingmall.exception.LeafServerException;
import com.tuling.tulingmall.exception.NoKeyException;
import com.tuling.tulingmall.service.IDGenerateService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
@RefreshScope
public class LeafController {
    private Logger logger = LoggerFactory.getLogger(LeafController.class);
    @Autowired
    @Qualifier("SegmentIDGenerateServiceImpl")
    private IDGenerateService idGenerateService;
    /**
     * 获取单个id
     */
    @RequestMapping(value = "/segment/get/{key}")
    public String getId(@PathVariable("key") String key) {
        if (key == null || key.isEmpty()) {
            throw new NoKeyException();
        }
        Result result = idGenerateService.getId(key);
        if (result.getStatus().equals(Status.EXCEPTION)) {
            throw new LeafServerException(result.toString());
        }
        return String.valueOf(result.getId());
    }
    @RequestMapping(value = "/segment/getlist/{key}")
    public List<String> getIds(@PathVariable("key") String key, @RequestParam int keyNumber) {
        if (keyNumber == 0 || keyNumber > 5000) keyNumber = 5000;
        if (key == null || key.isEmpty()) {
            throw new NoKeyException();
        }
        List<Result> results = idGenerateService.getIds(key, keyNumber);
        if (CollectionUtils.isEmpty(results)) {
            throw new LeafServerException("获得id列表失败！");
        }
        List<String> ids = new ArrayList<>();
        for (Result result : results) {
            if (result.getStatus().equals(Status.EXCEPTION)) {
                logger.error("获得id异常：", new LeafServerException(result.toString()));
            } else {
                ids.add(String.valueOf(result.getId()));
            }
        }
        return ids;
    }
}
