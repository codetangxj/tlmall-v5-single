package com.tuling.tulingmall.controller;

import com.tuling.leaf.core.common.Result;
import com.tuling.leaf.core.common.Status;
import com.tuling.tulingmall.exception.LeafServerException;
import com.tuling.tulingmall.service.IDGenerateService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class SnowflakeController {
    private Logger logger = LoggerFactory.getLogger(SnowflakeController.class);
    @Autowired
    @Qualifier("SnowflakeIDGenerateService")
    private IDGenerateService idGenerateService;
    @RequestMapping(value = "/snowflake/get")
    public String getId() {
        Result result = idGenerateService.getId(null);
        if (result.getStatus().equals(Status.EXCEPTION)) {
            throw new LeafServerException(result.toString());
        }
        return String.valueOf(result.getId());
    }
}
