package com.tuling.tulingmall.service.impl;

import com.tuling.leaf.core.IDGen;
import com.tuling.leaf.core.common.Result;
import com.tuling.leaf.core.segment.SegmentIDGenImpl;
import com.tuling.leaf.core.segment.dao.impl.IDAllocDaoImpl;
import com.tuling.tulingmall.exception.InitException;
import com.tuling.tulingmall.service.IDGenerateService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("SegmentIDGenerateServiceImpl")
public class SegmentIDGenerateServiceImpl implements IDGenerateService {
    private Logger logger = LoggerFactory.getLogger(SegmentIDGenerateServiceImpl.class);
    /**
     * id生成器
     */
    private IDGen idGen;

    public SegmentIDGenerateServiceImpl(IDAllocDaoImpl dao) throws InitException {
        idGen = new SegmentIDGenImpl();
        ((SegmentIDGenImpl) idGen).setDao(dao);
        if (idGen.init()) {
            logger.info("Segment Service Init Successfully");
        } else {
            throw new InitException("Segment Service Init Fail");
        }
    }

    @Override
    public Result getId(String key) {
        return idGen.get(key);
    }

    @Override
    public List<Result> getIds(String key, int keyNumber) {
        List<Result> results = new ArrayList<>();
        for (int i = 1; i <= keyNumber; i++) {
            results.add(idGen.get(key));
        }
        return results;
    }

    public SegmentIDGenImpl getIdGen() {
        if (idGen instanceof SegmentIDGenImpl) {
            return (SegmentIDGenImpl) idGen;
        }
        return null;
    }
}
