package com.tuling.tulingmall.service.impl;


import com.tuling.leaf.core.common.Result;
import com.tuling.leaf.core.common.Status;
import com.tuling.tulingmall.service.IDGenerateService;
import com.tuling.snowflake.core.SnowflakeIdWorker;
import org.apache.curator.framework.CuratorFramework;
import org.apache.zookeeper.CreateMode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Service("SnowflakeIDGenerateService")
public class SnowflakeIDGenerateServiceImpl implements IDGenerateService {
    private Logger logger = LoggerFactory.getLogger(SnowflakeIDGenerateServiceImpl.class);
    private SnowflakeIdWorker snowflakeIdWorker;
    @Autowired
    private CuratorFramework zkClient;
    private static String path = "/persistentSequential/workerId";
    @Value("${id.generate.lastTimestamp.persist.file.name}")
    private String persistLastTimestampFileName;


    @PostConstruct
    public void init() throws Exception {
        // localWorkerId的作用：如果zookeeper不可用，可以使用本地的workerId
        String localWorkerId = loadLocalWorkerId();
        String workerId = null;
        // 本地的workerId没有在zookeeper注册：先注册一个新的workerId，然后保存到本地
        if (zkClient.checkExists().forPath(path + localWorkerId) == null) {
            String newPath = zkClient.create()
                    .creatingParentContainersIfNeeded()
                    .withMode(CreateMode.PERSISTENT_SEQUENTIAL)
                    .forPath(path);
            // 创建的持久化节点不是纯粹的数字，需要重path总提取出数字部分
            workerId = extractSequenceNumber(newPath, path);
            localWorkerId = workerId;
        }
        // localWorkerId已注册
        else {
            /**
             * workerID已存在，如果是其他节点占用了这个Id怎么办？（如何规避多个节点抢占同一个id）
             * localWorkerId怎么来？
             * 1、启动时指定一个id：机器多的时候，指定不过来
             * 2、从zookeeper创建
             * 这里可以选择注册顺序节点来分配id
             * */
        }
        saveLocalWorkerId(localWorkerId);
        snowflakeIdWorker = new SnowflakeIdWorker(Long.valueOf(workerId), 2L,persistLastTimestampFileName);
    }

    /**
     * 从完整路径中截取出顺序号部分
     *
     * @param fullPath 顺序节点的完整路径
     * @param prefix   顺序节点的前缀
     * @return 截取到的顺序号部分，如果找不到则返回null
     */
    public static String extractSequenceNumber(String fullPath, String prefix) {
        // 检查路径是否以已知前缀开始
        if (fullPath.startsWith(prefix)) {
            // 从前缀后的第一个字符开始截取到字符串末尾
            return fullPath.substring(prefix.length());
        } else {
            // 如果路径不以已知前缀开始，则返回null或抛出异常（取决于你的需求）
            return null;
        }
    }

    private String loadLocalWorkerId() {
        // 可以从磁盘读取这个id
        return null;
    }

    private String saveLocalWorkerId(String localWorkerId) {
        // 将localWorkerId写入磁盘
        return null;
    }

    @Override
    public Result getId(String key) {
        return new Result(snowflakeIdWorker.nextId(), Status.SUCCESS);
    }

    @Override
    public List<Result> getIds(String key, int keyNumber) {
        List<Result> results = new ArrayList<>();
        for (int i = 0; i < keyNumber; i++) {
            results.add(getId(key));
        }
        return results;
    }
}
