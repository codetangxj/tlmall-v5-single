package com.tuling.tulingmall.service;

import com.tuling.leaf.core.common.Result;

import java.util.List;

public interface IDGenerateService {
    Result getId(String key);

    List<Result> getIds(String key, int keyNumber);
}
