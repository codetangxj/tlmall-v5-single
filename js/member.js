const http = require('http');

const options = {
    hostname: '127.0.0.1',
    port: 8877,
    path: '/sso/getAuthCode?telephone=110',
    method: 'GET'
};

const req = http.request(options, (res) => {
    let data = '';
    res.on('data', (chunk) => {
        data += chunk;
    });
    res.on('end', () => {
        console.log(data);
    });

});

// 错误处理
req.on('error', (e) => {
    console.error(`problem with request: ${e.message}`);
});

// 结束请求
req.end();