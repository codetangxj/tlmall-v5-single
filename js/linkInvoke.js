function asyncOperation1() {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            console.log('asyncOperation1 completed');
            resolve();
        }, 1000);
    });
}

function asyncOperation2() {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            console.log('asyncOperation2 completed');
            resolve();
        }, 1000);
    });
}

function asyncOperation3() {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            console.log('asyncOperation3 completed');
            resolve();
        }, 1000);
    });
}

// 使用then来实现链式调用
asyncOperation1()
    .then(() => asyncOperation2())
    .then(() => asyncOperation3())
    .then(() => console.log('All operations completed'));