const http = require('http');

new Promise(function (resolve, reject) {
    const options = {
        hostname: '127.0.0.1',
        port: 8877,
        path: '/sso/getAuthCode?telephone=110',
        method: 'GET'
    };
    const req = http.request(options, (res) => {
        let result = '';
        res.on('data', (chunk) => {
            result = JSON.parse(chunk);
        });
        res.on('end', () => {
            console.log(result);
            resolve(result.data);
        });

    });
    req.on('error', (e) => {
        console.error(`problem with request: ${e.message}`);
    });
    req.end();
}).then(function (authCode) {
    console.log('authCode',authCode)
    const data = JSON.stringify({
        key: 'value'
    });

    const options = {
        hostname: '127.0.0.1',
        port: 8877,
        path: '/sso/login?username=monkey&password=123456&verifyCode='+authCode,
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        }
    };
    const req = http.request(options, (res) => {
        res.setEncoding('utf8');
        let response = '';
        res.on('data', (chunk) => {
            response += chunk;
        });
        res.on('end', () => {
            console.log(response);
        });
    });

    req.on('error', (e) => {
        console.error(`problem with request: ${e.message}`);
    });
    req.write(data);
    req.end();
})